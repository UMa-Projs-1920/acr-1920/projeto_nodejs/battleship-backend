const GameModel = require('../models/Game');
const MessageModel = require('../models/Message');
const UserModel = require('../models/User');
const mongoose = require('mongoose');
const BOARD_SIZE = 10;



async function  createGame(creator) {

    let game = new GameModel({
        creator,
        creatorBoard: initBoard(),
        opponentBoard: initBoard(),
    });

    return await game.save();
}



async function getGames(user) {
    return GameModel.find({$or : [{"creator" : user }, {"opponent" : user}] });


}

async function getGame(gameId) {
    return await GameModel.findById(gameId);
}

async function joinGame(gameId, user) {
    
    if(!mongoose.Types.ObjectId.isValid(gameId))
        return false;

    let game = await GameModel.findById(gameId);

    if(game == null)
        return null;

    if(game.opponent == null && game.creator != user) {
        await GameModel.updateOne({_id: gameId} , {opponent : user})
        game = await GameModel.findById(gameId);
        return game;
    }

    if(game.creator == user || game.opponent == user ) {
        return game;
    }

    return null;
}


async function removeGame(gameId, user) {
    let game = await GameModel.findById(gameId);

    if(game.creator == user) {
        await GameModel.deleteOne({ _id: gameId });
        await MessageModel.deleteMany({room: gameId})
    }
}

async function setShips(gameId, user, ships) {
    let game = await GameModel.findById(gameId);

    if(validateShips(ships)) {
        if(game.creator == user) {

            if(game.creatorShips.length) return; // TESTE

            let board = game.creatorBoard;

            drawShips(board, ships);
            await GameModel.updateOne({_id : gameId}, {creatorShips : ships, creatorBoard: board});
        } else {

            if(game.opponentShips.length) return; // TESTE

            let board = game.opponentBoard;

            drawShips(board, ships);



            await GameModel.updateOne({_id : gameId}, {opponentShips : ships, opponentBoard: board});

        }

    }

}

function validateShips(shipList) {

    let validationMap = [];
    let valid = false;

    shipList.forEach(element => {
        validationMap.push(element.ship);
    });

    valid = sumShips(validationMap, "carrier") == 1;
    valid &= sumShips(validationMap, "battleship") == 1;
    valid &= sumShips(validationMap, "cruiser") == 1;
    valid &= sumShips(validationMap, "submarine") == 1;
    valid &= sumShips(validationMap, "destroyer") == 1;

    return valid;
}

function sumShips(map, ship) {
    return map.reduce(function(acc,val)  {
        return val == ship ? acc + 1 : acc + 0 ;
    }, 0);
}

async function getShips(gameId , user) {
    let game = await GameModel.findOne({ _id : gameId });

    if(game.creator == user) {
        return game.creatorShips;
    } else {
        return game.opponentShips;
    }
}


function initBoard() {
    let board = new Array();

    for(let y = 0; y < BOARD_SIZE; y++) {

        row = new Array();

        for(let x = 0; x < BOARD_SIZE; x++) {
            row.push( {
                hit: false,
                me: false
            });
        }

        board.push(row);
    }

    return board;
}
function drawShips(playerBoard, shipList) {
   

    shipList.forEach((ship) => {
        drawShip(playerBoard, ship);
    });

}

function drawShip(playerBoard, ship) {


    let xPos = ship.x;
    let yPos = ship.y;
    let direction = ship.dir;
    let size = ship.size;

    if(direction == "Horizontal") {
      for(let x = xPos ; x < xPos + size ; x++){
          playerBoard[yPos][x].me = true;
      }
    } else {
      for(let y = yPos ; y < yPos + size ; y++){
          playerBoard[y][xPos].me = true;
      }
    }

}

async function recoverState(gameId, user) {
    let game = await GameModel.findOne({ _id : gameId }).select("-opponentShips");

    if(game.creator == user) {
        hideGameBoard(game.creatorBoard);
    } else {
        hideGameBoard(game.opponentBoard);
    }

    return game;
}

function hideGameBoard(game, user){

    let gameBoard;
    let shipList
    
    if(game.creator == user) {
        gameBoard = game.opponentBoard;
        shipList = game.opponentShips;
    } else {
        gameBoard = game.creatorBoard;
        shipList = game.creatorShips;
    }

    for(ship of shipList ) {
        delete ship.x;
        delete ship.y;
        delete ship.dir;

    }



    gameBoard.forEach(row => {
        row.forEach(col => {
            if(!col.hit)
                col.me = false;
        })
    })
}

function isReady(game) {
   return game.opponentShips.length > 0 && game.creatorShips.length > 0;
}

async function setTurn(gameId, user) {
    return await GameModel.findOneAndUpdate({_id: gameId} , {status: "playing",turn : user},{"new": true})
}

async function shot(gameId, user, x, y) {
    let game = await getGame(gameId);
    let hit = false;

    if(game.creator == user) {
        hit = checkHit(x,y, game.opponentShips);
    } else {
        hit = checkHit(x,y, game.creatorShips);
    }

    await updateShot(gameId, user , x , y);

    return hit;
}

async function updateShot(gameId, user , x ,y) {

    let game = await getGame(gameId);
    let board;

    if(game.creator == user) {
         board = drawShot(game.opponentBoard, x, y);



        await GameModel.updateOne({_id: gameId} , {opponentBoard : board});

    } else {
        board = drawShot(game.creatorBoard, x, y);
        await GameModel.updateOne({_id: gameId} , {creatorBoard : board});
    }


}

function drawShot(board, x, y) {
    board[y][x].hit = true;
    return board;
}



function checkHit(x,y,shipList) {

    for(ship of shipList) {

        if(ship.dir == "Horizontal") {
            if(ship.y == y && (x >= ship.x && x < ship.x + ship.size))
                return true;
        } else {
            if(ship.x == x && (y >= ship.y && y < ship.y + ship.size ))
                return true;
        }

    }

    return false;

}

async function detectSink(gameId, user, xPos, yPos, callback) {
    let game = await getGame(gameId);

    let board;
    let shipList;

    if(game.creator == user) {
        board = game.opponentBoard;
        shipList = game.opponentShips;
    } else {
        board = game.creatorBoard;
        shipList = game.creatorShips;
    }

   let ship = detectShip(shipList, xPos , yPos); 
   
   let sink = null;

   if(ship) {

        sink = true;
        if(ship.dir == "Horizontal") {
                let delta = ship.x  + ship.size;

                for(let x = ship.x; x < delta; x++) {
                    sink &= board[yPos][x].hit;

                }

        } else {

                let delta = ship.y  + ship.size;

                for(let y = ship.y; y < delta; y++) {
                    sink &= board[y][xPos].hit;
                }

        }
       
    } 

    if(sink) {

        if(game.creator == user) {
            
            await GameModel.updateOne({_id : gameId , 'opponentShips.ship' : ship.ship} , {
                "$set" : {'opponentShips.$.sink': true}
            })
        } else {
            await GameModel.updateOne({_id : gameId , 'creatorShips.ship' : ship.ship} , {
                "$set" : {'creatorShips.$.sink': true}
            })
        }

    }

    callback(sink ? ship : null);

}



function detectShip(shipList, xPos, yPos) {


    let hitShip = null;

    for(ship of shipList) {
        if(ship.dir == "Horizontal") {
            if(ship.y == yPos && (xPos >= ship.x && xPos < ship.x + ship.size))
                hitShip = ship;
        } else {
            if(ship.x == xPos && (yPos >= ship.y && yPos < ship.y + ship.size))
                hitShip = ship;
        }
    }


    return hitShip;
}

async function detectEndGame(gameId, callback) {
    let game = await getGame(gameId);

    if (game.winner) callback(null);

    let creatorSink = sumSink(game.creatorShips);
    let opponentSink = sumSink(game.opponentShips);
    let winner, looser;
    let winnerBoard, looserBoard;

    if(creatorSink == 5) {

        await GameModel.updateOne({_id : gameId}, {status: "ended", winner : game.opponent})
        winner = game.opponent;
        looser = game.creator;
        winnerBoard = game.opponentBoard;
        looserBoard = game.creatorBoard;
    }
    
    if (opponentSink == 5) {
        await GameModel.updateOne({_id : gameId}, {status: "ended", winner : game.creator})
        winner = game.creator;
        looser = game.opponent;
        winnerBoard = game.creatorBoard;
        looserBoard = game.opponentBoard;
    }

    if(winner) {

        let winnerData = sumShotsAndHits(winnerBoard);
        let looserData = sumShotsAndHits(looserBoard);

        await UserModel.updateOne({username : winner}, {$inc: {victorys : 1 , gamesPlayed : 1, hits: looserData.hits , shots: looserData.shots }});
        await UserModel.updateOne({username : looser}, {$inc: {gamesPlayed : 1, hits: winnerData.hits ,  shots: winnerData.shots }});

        callback(winner);
    } 

    callback(null);

}

function sumShotsAndHits(gameboard) {
    let shots = 0;
    let hits = 0;

    for(let y = 0; y < BOARD_SIZE;  y++) {
        for(let x = 0; x < BOARD_SIZE; x++) {
            shots += gameboard[y][x].hit ? 1 : 0;
            hits +=  (gameboard[y][x].hit && gameboard[y][x].me) ? 1 : 0;
        }
    }

    return {shots, hits};
}

function sumSink(shipList) {
    let sum = 0;
    for(ship of shipList) {
        if(ship.sink)
            sum++;
    }

    return sum;
}

async function alreadyHit(gameId, user, x, y) {
    let game = await getGame(gameId);
    let board;

    if(game.creator == user) {
        board = game.opponentBoard;
    } else {
        board = game.creatorBoard;
    }


    return  board[y][x].hit;

}

module.exports = {
    createGame,
    joinGame,
    removeGame,
    getGame,
    getGames,
    setShips,
    getShips,
    hideGameBoard,
    isReady,
    setTurn,
    shot,
    detectSink,
    detectEndGame,
    alreadyHit

}