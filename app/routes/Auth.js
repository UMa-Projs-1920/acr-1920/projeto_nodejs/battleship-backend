const AuthController = require('../controllers/Auth');
const express = require('express');
const router = express.Router();
const passport = require('passport');
/*
router.post('/login',(req, res) => {
    console.log("LOGIN ATTEMPT")
    passport.authenticate('local', function(err,user, info) {
        console.log("AUTH ATTEMPT");
        if(err)
            console.error(err);

        console.log(info)

        if(!user) {
            console.log("user err")
            res.render('login', {error : "credentials"})
        } else {
            res.redirect("/game");
        }
    })
})
*/

router.get('/login', (req,res) => {
    if(req.isAuthenticated())
        return res.redirect('/game/');

    res.render('login',{req});
});


router.post('/login', function(req, res, next) {



    passport.authenticate('local', function(err, user, info) {
        if (err) {
           console.error(err)
           next(err)
        }
        res.isAuthenticated

      if (!user) { return res.render('login', {error:"bad_credentials", req}); }
      req.logIn(user, function(err) {
        if (err) { return next(err); }

        if(req.session.afterAuthRedirect) {
            return res.redirect(req.session.afterAuthRedirect);
        }
        
        return res.redirect('/game/');
      });


    })(req, res, next);
  });


router.get('/register', (req, res) => {
    res.render('register',{req});
})

router.post('/register', async (req, res) => {
    let {username , email, password, rpassword} = req.body;

    let errors = [];

    if(username.length == 0)
        errors.push("O campo username está vazio.")

    if(username.length > 0 && username.length < 3)
        errors.push("O username escolhido deve ser maior que 3 caracteres.")

    if(email.length == 0)
        errors.push("O campo email está vazio.")

    if(password.length == 0)
        errors.push("O campo password está vazio.")

    if(password.length <= 5)
        errors.push("A password deve ter mais de 5 caracteres.")

    if(password.length == 0)
        errors.push("O campo repetir password está vazio.")

    if(password != rpassword)
        errors.push("As passwords não coincidem.")

    if(errors.length > 0)
        return res.render('register',{errors, body : req.body ,req})

    let userExists = await AuthController.findUser(username);

    if(userExists) {
        errors.push("O username já se encontra ocupado!")
        return res.render('register',{errors, body : req.body, req})
    }

    AuthController.createUser({
        username,
        password,
        email
    })
    .then(() => {
        req.login({username},(err) => {
            if(err)
                return res.render('register', {result: "general_error", req})
            res.redirect('/game/')
        })
    })
    .catch((err) => {
        console.log("ERROR", err);
        return res.render('register', {result: "general_error", req})
    })

})

router.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});



module.exports = router;