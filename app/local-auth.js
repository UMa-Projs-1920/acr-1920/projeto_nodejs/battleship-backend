const LocalStrategy = require('passport-local').Strategy
const AuthController = require('./controllers/Auth');
const bcrypt = require('bcrypt')

function init(passport) {
    const authenticate = async (username, password, done) => {
        let user = await AuthController.findUser(username,true);

        if(user) {
            bcrypt.compare(password, user.password, (err,valid) => {
                if(err) {
            
                    return done(err)
                }
                  

                if (!valid) {
                    return done(null, false)
                }
                
                delete user.password;
                return done(null, user)
            })

        } else {
            return done(null, false);
        }

    }

    passport.use('local',new LocalStrategy({usernameField: 'username', passwordField: 'password'},authenticate));
    passport.serializeUser((user, done) => {done(null, user.username)

    });
    passport.deserializeUser(async (id, done) => {done(null,await AuthController.findUser(id,false))});



}

function authenticated(req,res,next)  {
    if(req.user) {
        next();
    } else {
        req.session.afterAuthRedirect = req.originalUrl;
        res.redirect("/login",);
    }
}

module.exports = {init, authenticated };