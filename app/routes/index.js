const express = require('express');
const auth = require('./Auth');
const api = require('./Api');
const router = express.Router();

router.use(auth);
router.use('/api',api);

module.exports = router;