const socketio = require('socket.io');
const messagesModel = require('./models/Message'); // Substituir pelo Controller
const GameController = require('./controllers/Game');


let io;




function messages() {

    io.on('connection', (socket) => {

        var gameId;

        /*
            Entrar no jogo (em qualquer altura)
        */
        socket.on("join", async room => {

            const user= socket.request.session.passport.user;

            game = await GameController.joinGame(room, user);

            if(game) {

                gameId = game._id;
                socket.join(room);


                let msgs = await messagesModel.find({room: room});

                
                msgs.forEach(el => {
                    socket.emit("history",{user: el.user, message: el.message}); // histórico do chat
                })

                GameController.hideGameBoard(game, user); // Proteger os dados do adversário

                socket.emit('joined', game) // enviar dados do jogo
                io.to(room).emit("chat", { message: `O utilizador ${user} entrou no jogo.`}) // enviar mensagem de user Joined

            } else {
                socket.emit("gameError", "Not Authorized");
            }
            
        });

        socket.on("ready", async function (shipList) {
            if(!gameId) {

            }
            
            if(socket.request.session.passport) {
                const user = socket.request.session.passport.user;
                
                await GameController.setShips(gameId,user,shipList);

                let game = await GameController.getGame(gameId);

            
                // user ready
                io.to(gameId).emit("ready", user);
                io.to(gameId).emit("chat", { message: `O utilizador ${user} está pronto.`}) 

                if(GameController.isReady(game)) {
   
                    if(!game.turn) {
                        let newTurn = randomUser(game.creator, game.opponent);
                        game = await GameController.setTurn(gameId,newTurn )
                        io.to(gameId).emit("chat", { message: `O jogo vai ter início. O utilizador ${newTurn} começa.`});
                    }

                    io.to(gameId).emit("play", game.turn);
                }


            }
            
        });

        socket.on("leaveGames", () => {

            var rooms = io.sockets.adapter.sids[socket.id];
            
            Object.keys(rooms).forEach(room => {
                if(room != socket.id)
                    socket.leave(room);
            })
  
        });

        socket.on("shot", async function(x,y) {
            if(socket.request.session.passport) {
                const user = socket.request.session.passport.user;
                let game = await GameController.getGame(gameId);

                if(game.turn == user) {

                    let hit = false;
                    let nextTurn;

                    if(game.creator == user) {
                       nextTurn = game.opponent;
                    } else {
                        nextTurn = game.creator;
                    }


                    await GameController.shot(gameId, user, x, y) ? hit = true : hit = false;

                    await GameController.setTurn(gameId, nextTurn);
                    io.to(gameId).emit("shotResult", {next: nextTurn, user: user, hit, x, y});

                    GameController.detectSink(gameId, user, x, y, (sink) => {
                        if(sink) {
                            socket.emit('sink', sink.ship);
                            io.to(gameId).emit("chat", { message: `O utilizador ${user} afundou o ${prettyShipName(sink.ship)} de ${nextTurn}.`});
                        }

                        GameController.detectEndGame(gameId,async (winner) => {
                            if(winner) {
                                io.to(gameId).emit("chat", { message: `O utilizador ${winner} venceu o jogo.`});

                                let game = await GameController.getGame(gameId);
                                io.to(gameId).emit("winner", winner, game);
                            }
                        })
                    });

                 


                } else {

                }
            }
        });

        socket.on("chat", async (room,message) => {
            const user= socket.request.session.passport.user;
            //console.log(`Mensagem recebida na ${room} de ${user}: ${message}`)
            io.to(room).emit("chat",{user, message});

            let msg = new messagesModel({
                user,
                room,
                message
            });

            await msg.save();

        });

        socket.on('disconnect', function(){
            if(socket.request.session.passport) {
                const user= socket.request.session.passport.user;
                io.to(gameId).emit("chat", { message: `O utilizador ${user} saiu do jogo. Poderá recomeçar mais tarde.`});
            }
        });



    })

}


function randomUser(firstUser, anotherUser) {
    let val = Math.round(Math.random() * 99) + 1;

    return val > 50 ? anotherUser : firstUser;
}


function prettyShipName(shipName) {
    switch (shipName) {
        case 'carrier' : return "Porta-Aviões";
        case 'battleship' : return "Couraçado";
        case 'cruiser' : return "Cruzador";
        case 'submarine' : return "Submarino";
        case 'destroyer' : return "Contra-torpedeiro";
        default: return "";
    }
}
module.exports = {
    startSocket : (server) => {
        io = socketio(server);
        return io;
    },

    use : (middleware) => {
        io.use(middleware);
    },

    init() {
        messages();
    }
}