const mongoose = require('mongoose');


let messageSchema = mongoose.Schema({
    "user" : {
        type : String,
        required : true
    },
    "room" : {
        type : String,
        required: true
    },
    "message" : {
        type: String,
        required: true,
    }
    

});


module.exports = mongoose.model("messages",messageSchema);