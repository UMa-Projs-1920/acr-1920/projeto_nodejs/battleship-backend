const express = require('express');
const router = express.Router();
const { authenticated } = require('../local-auth'); // middleware de authenticação
const GameController = require('../controllers/Game');
const RankingController = require('../controllers/Ranking');
const AuthController = require('../controllers/Auth');
const mongoose = require ('mongoose');


router.get('/',authenticated, (req,res) => {
    res.json(req.user);
})


router.get('/user' , authenticated , (req, res) => {
    res.json(req.user);
})

router.get('/auth', (req, res) => {
    res.json(req.user ? {auth: true} : {auth: false});
})


router.post('/user/password', authenticated, async (req,res) => {
    const {newPassword, oldPassword} = req.body;
    const user = req.user;

    if(newPassword && newPassword.length > 5) {
        if(await AuthController.setPassword(user._id, newPassword, oldPassword)) {
            res.json({ "newPassword" : "OK"});
        } else {
            res.json({ error: "INVALID_OLD"});
        }

    } else {
        res.json({ error: "PWD_LEN"});
    }
});

// Criar novo jogo
router.post('/game', authenticated , async (req, res) => {
    const user = req.user;
    const creator = user.username;

    let data = await GameController.createGame(creator);

    res.json(data);
});

router.delete('/game/:id',authenticated, async (req,res) => {
    const user = req.user.username;
    const gameId = req.params.id;


    if(mongoose.Types.ObjectId.isValid(gameId))
        await GameController.removeGame(gameId,user);

    res.send();
});

router.get('/game', authenticated , async (req,res) => {

    res.json( await GameController.getGames(req.user.username) );
})

router.get('/ranking', async (req,res) => {
    res.json(await RankingController.getRanking());
})

router.get('/messages', async (req,res) => {
    res.json(await RankingController.getRanking());
})




module.exports = router;