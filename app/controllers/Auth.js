const UserModel = require('../models/User');
const bcrypt = require('bcrypt');


async function findUser(username,password,callback){
    if(password == true) {
        return UserModel.findOne({username}).select('+password').exec();
    }
      
    return UserModel.findOne({username}).exec();
}

function createUser(user) {

    let newUser = new UserModel({
        "username" : user.username,
        "password" : user.password,
        "email" : user.email
    });
    return newUser.save();
}


async function setPassword(userId, newPassword, oldPassword) {
    let user =  await UserModel.findOne({_id: userId }).select("+password");

    if(bcrypt.compareSync(oldPassword,user.password)) {

        let newHash = bcrypt.hashSync(newPassword,Number(process.env.HASH_FORCE) || 10);
        let update = await UserModel.updateOne({_id : userId}, {password : newHash});
        if(update.nModified > 0)
            return true;

        
    }

    return false;
 
} 

module.exports = {
    findUser,
    createUser,
    setPassword
}