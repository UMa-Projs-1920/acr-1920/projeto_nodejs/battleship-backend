const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

let userSchema = mongoose.Schema({
    "username" : {
        type : String,
        unique: true,
        required: true,
        minlength: 3
    },
    "email" : {
        type:String,
        required: true
    },
    "password" : {
        type: String,
        required: true,
        select: false
    },
    "gamesPlayed" : {
        type: Number,
        default: 0
    },
    "victorys": {
        type: Number,
        default: 0
    },
    "hits": {
        type: Number,
        default: 0
    },
    "shots": {
        type: Number,
        default: 0
    },
    
});

userSchema.pre('save',  function (next) {
    if(!this.isModified('password')) {
        return next();
    }


    this.password =  bcrypt.hashSync(this.password,Number(process.env.HASH_FORCE) || 10)
    next();
})

module.exports = mongoose.model("users",userSchema);