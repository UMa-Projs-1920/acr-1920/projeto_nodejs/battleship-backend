require('dotenv').config(); // Load .env file
const express   = require('express');
const history = require('connect-history-api-fallback');
const http      = require('http');
const routes    = require('./routes');
const ejs       = require('ejs');
const websocket = require('./socket');
const mongoose = require('mongoose');
const session = require('express-session');
const passport = require('passport');
const localauth = require('./local-auth');
const initPassport = localauth.init;
const path = require('path');


const app = express();
const PORT = process.env.PORT || 3000;
const server = http.createServer(app);


const MONGO_SERVER = process.env.MONGO_SERVER;


app.set('view engine', 'ejs');
app.set('views', './public');






app.use(function (req, res, next) {
    res.set("Access-Control-Allow-Origin" ,"*")
    res.set("Access-Control-Allow-Headers" ,"*")
    next()
});

let sessionMiddleware = session({
    secret : process.env.SECRET,
    domain : 'localhost',
    resave: false,
    saveUninitialized: false
});

app.use(sessionMiddleware);

app.use(express.json());
app.use(express.urlencoded({extended : false}));
app.use(passport.initialize());
app.use(passport.session());
initPassport(passport);
app.use(routes);






websocket.startSocket(server ); 

websocket.use(function(socket, next){
    sessionMiddleware(socket.request, {}, next); 
});

websocket.init();

app.use("/css",express.static(__dirname + '/../public/css'));
app.use("/js",express.static(__dirname + '/../public/js'));
app.use("/img",express.static(__dirname + '/../public/img'));
app.use("/sounds",express.static(__dirname + '/../public/sounds'));

app.get('/', (req,res) => {
    res.render('index',{req});
});

app.use("/game", history({}), localauth.authenticated, express.static(__dirname + '/../public/game'))

app.use('/favicon.ico', express.static(path.join(__dirname, '..' ,'public/favicon.ico')));


app.get('*', (req,res) => {
    res.status(404).render('404',{req});
})






mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);


mongoose.connect(MONGO_SERVER)
.then(() => {
    console.log("Connected to MONGODB Database!");
    server.listen(PORT,() => {
        console.log(`Battleship serve running on port ${PORT}`);
    })
})
.catch((err) => {
    console.error("MongoDB Connection ERROR!")
    console.error("Error:", err)
})

