const UserModel = require('../models/User');


async function getRanking(){

    let ranking = await UserModel.find({}).select('username victorys $position').sort({'victorys': -1});

    return ranking.map( (elm, index) => {
       return { ...elm.toObject() , position : index +1};
    })
    
}



module.exports = {

    getRanking
}