const mongoose = require('mongoose');


let gameSchema = mongoose.Schema({
    "creator" : {
        type : String,
        required : true
    },
    "opponent" : {
        type : String
    },
    "status" : {
        type: String,
        enum: ['created','playing','ended'],
        default: 'created'
    },
    "creatorBoard" : {
        type: Array
    },
    "creatorShips" : {
        type: Array
    },
    "opponentBoard": {
        type: Array
    },
    "opponentShips" : {
        type: Array
    },
    "turn" : {
        type: String
    },
    "winner" : {
        type: String
    }
    

},{
    timestamps: { createdAt : true}
});



module.exports = mongoose.model("games",gameSchema);